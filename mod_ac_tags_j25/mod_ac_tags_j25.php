<?php
// Prohibir acceso directo
defined('_JEXEC') or die ('A molestar a otro lado');

//jimport('joomla.log.log');
//JLog::addLogger(array('text_file' => 'mod_ac_tags_j25.php'), JLog::ALL, array('mod_ac_tags_j25'));

// Incluir archivo AUXILIAR
require_once(dirname(__FILE__).DS.'helper.php');

// Obtener NOMBRE del módulo
$path_parts = pathinfo(__FILE__);
$module_name = $path_parts['filename'];
//JLog::add('Nombre: ['.$module_name.']');
// Obtener DATOS a desplegar
$modHelper = new ModAcTagsJ25Helper();
$items = $modHelper->getItems($params, $module_name);

// Invocar la PLANTILLA
require(JModuleHelper::getLayoutPath($module_name));

